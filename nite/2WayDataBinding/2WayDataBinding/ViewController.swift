//
//  ViewController.swift
//  2WayDataBinding
//
//  Created by Valerian on 01/10/2021.
//

import UIKit

//Obserable
class Observable<T> {
    var value : T? {
        didSet {
            listeners.forEach {
                $0!(value)
            }
        }
    }
    
    init(_ value : T?) {
        self.value = value
    }
    
    private var listeners : [((T?) -> Void)?] = []
    
    func bind(_ listener: @escaping (T?) -> Void) {
        listener(value)
        self.listeners.append(listener)
    }
}
//Model

struct user: Codable {
    var name: String
}

//ViewModels

struct UserListViewModel {
    var user : Observable<[UserTableViewCellViewModel]> = Observable([])
}

struct UserTableViewCellViewModel {
    let name: String
}

//Controller
class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    private let tableView: UITableView = {
        let table = UITableView()
        table.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        return table
    }()
    
    private var viewModel = UserListViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(tableView)
        tableView.frame = view.bounds
        tableView.dataSource = self
        tableView.delegate = self
        
        viewModel.user.bind { [weak self] _ in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
        
        fetch()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.user.value?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = viewModel.user.value?[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func fetch() {
        
        let nameDataFake = ["tien", "tien", "tien", "tien", "tien", "tien", "tien", "tien", "tien"]
        
        //        for item in nameDataFake {
        //            viewModel.user.value?.append(UserTableViewCellViewModel(name: item))
        //        }
        
        nameDataFake.forEach {
            viewModel.user.value?.append(UserTableViewCellViewModel(name: $0))
        }
        //        guard let url = URL(string: "https://jsonplaceholder.typicode.com/user") else { return }
        //        let task = URLSession.shared.dataTask(with: url) { data, _ , error in
        //            guard let data = data else { return }
        //            do {
        //                let userModel = try JSONDecoder().decode([user].self, from: data)
        //                self.viewModel.user.value = userModel.compactMap({
        //                    UserTableViewCellViewModel(name: $0.name)
        //                })
        //            } catch {
        //                print(error.localizedDescription)
        //            }
        //        }
        //        task.resume()
    }
}

