//
//  ViewController.swift
//  CombineCollectionView
//
//  Created by Valerian on 30/09/2021.
//

import UIKit

extension NSObject {

    func background(delay: Double = 0.0, background: (()->Void)? = nil, completion: (() -> Void)? = nil) {
        DispatchQueue.global(qos: .background).async {
            background?()
            if let completion = completion {
                DispatchQueue.main.asyncAfter(deadline: .now() + delay, execute: {
                    completion()
                })
            }
        }
    }
    
    var className: String {
        
        return String(describing: type(of: self))
    }
    
    class var className: String {
        
        return String(describing: self)
    }

}


class ViewController: UIViewController {

    @IBOutlet weak var CombineCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CombineCollectionView.register(UINib(nibName: HeaderHomeCollectionReusableView.className, bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: HeaderHomeCollectionReusableView.className)
        CombineCollectionView.register(UINib(nibName: FooterHomeCollectionReusableView.className, bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: FooterHomeCollectionReusableView.className)
        CombineCollectionView.register(item1.nib(), forCellWithReuseIdentifier: item1.identifier)
        CombineCollectionView.register(item2.nib(), forCellWithReuseIdentifier: item2.identifier)
    }
}

extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }

    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionView.elementKindSectionHeader {
            let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: HeaderHomeCollectionReusableView.className, for: indexPath) as! HeaderHomeCollectionReusableView
            switch indexPath.section {
            case 0:
                header.nameLabel.text = "Header Title 1"
            case 1:
                header.nameLabel.text = "Header Title 2"
            default:
                break
            }
            return header
        } else {
            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: FooterHomeCollectionReusableView.className, for: indexPath) as! FooterHomeCollectionReusableView
            switch indexPath.section {
            case 0:
                footerView.nameLabel.text = "Footer Title 1"
            case 1:
                footerView.nameLabel.text = "Footer Title 2"
            default:
                break
            }
            return footerView
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: 80)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: 80)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: item1.identifier, for: indexPath) as! item1
            cell.labelName2.text = "1"
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: item2.identifier, for: indexPath) as! item2
            cell.labelName1.text = "2"
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width / 4.5, height: 200)
    }
}
