//
//  item2.swift
//  CombineCollectionView
//
//  Created by Valerian on 30/09/2021.
//

import UIKit

class item2: UICollectionViewCell {
    
    static let identifier = "item2"
    
    static func nib() -> UINib {
        return UINib(nibName: "item2", bundle: nil)
    }
    
    @IBOutlet weak var labelName1 : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
