//
//  item1.swift
//  CombineCollectionView
//
//  Created by Valerian on 30/09/2021.
//

import UIKit

class item1: UICollectionViewCell {
    
    static let identifier = "item1"
    
    static func nib() -> UINib {
        return UINib(nibName: "item1", bundle: nil)
    }
    
    @IBOutlet weak var labelName2 : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
