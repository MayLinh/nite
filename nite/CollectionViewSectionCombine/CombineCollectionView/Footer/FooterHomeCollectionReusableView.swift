//
//  FooterHomeCollectionReusableView.swift
//  CombineCollectionView
//
//  Created by Valerian on 30/09/2021.
//

import UIKit

class FooterHomeCollectionReusableView: UICollectionReusableView {
    
    @IBOutlet weak var nameLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
